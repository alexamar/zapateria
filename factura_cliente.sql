-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-09-2019 a las 20:36:50
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `zapateria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_cliente`
--

CREATE TABLE `factura_cliente` (
  `id` int(11) NOT NULL,
  `cajero_id` int(11) NOT NULL,
  `cliente_cedula` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `empresa_id` int(11) NOT NULL,
  `tipo_pago` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura_cliente`
--

INSERT INTO `factura_cliente` (`id`, `cajero_id`, `cliente_cedula`, `fecha`, `empresa_id`, `tipo_pago`) VALUES
(1, 42, 123, '2019-09-22 18:31:00', 1, 'Efectivo'),
(2, 42, 567, '2019-09-22 18:31:00', 1, 'Efectivo'),
(3, 42, 891, '2019-09-22 18:31:00', 1, 'Efectivo'),
(4, 42, 123, '2019-09-22 18:31:00', 1, 'Efectivo'),
(5, 42, 567, '2019-09-22 18:31:00', 1, 'Efectivo'),
(6, 42, 891, '2019-09-22 18:31:00', 1, 'Efectivo'),
(7, 42, 123, '2019-09-22 18:31:00', 1, 'Efectivo'),
(8, 42, 567, '2019-09-22 18:31:00', 1, 'Efectivo'),
(9, 42, 891, '2019-09-22 18:31:00', 1, 'Efectivo'),
(10, 42, 123, '2019-09-22 18:31:00', 1, 'Efectivo'),
(11, 42, 567, '2019-09-22 18:31:00', 1, 'Efectivo'),
(12, 42, 891, '2019-09-22 18:31:00', 1, 'Efectivo'),
(13, 42, 123, '2019-09-22 18:31:00', 1, 'Efectivo'),
(14, 42, 567, '2019-09-22 18:31:00', 1, 'Efectivo'),
(15, 42, 891, '2019-09-22 18:31:00', 1, 'Efectivo'),
(16, 42, 123, '2019-09-22 18:31:00', 1, 'Efectivo');


--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `factura_cliente`
--
ALTER TABLE `factura_cliente`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `factura_cliente`
--
ALTER TABLE `factura_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
