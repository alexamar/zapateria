<?php

Auth::routes();

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/clear-cache', function() {
    
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
     
     $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    
});



        Route::middleware(['auth'])->group(function () {
        Route::get('/home', 'AdminController@index')->name('Administración');


        Route::prefix('cajero')->middleware(['CheckUser'])->group(function () {


        Route::get('producto','ProductoController@index')->name('producto.index');
        Route::get('producto/create','ProductoController@create')->name('producto.create');
        Route::post('producto/store','ProductoController@store')->name('producto.store');
        Route::post('producto/update','ProductoController@update')->name('producto.update');
        Route::get('producto/edit/{id}','ProductoController@edit')->name('producto.edit');
        Route::get('producto/buscar','ProductoController@buscar')->name('producto.search'); 

        Route::get('cierre','CierreController@index')->name('cierre.index');
    
        Route::get('venta','VentaController@index')->name('venta.index');
        Route::get('venta/agregar','VentaController@agregar')->name('venta.agregar');
        Route::get('venta/delete/{id}','VentaController@delete')->name('venta.delete');
        Route::post('venta/store','VentaController@store')->name('venta.store');
        Route::get('factura','VentaController@verFactura')->name('ver.factura');


        Route::get('/cliente/{id}', 'VentaController@GetCliente');
        

        });

    Route::prefix('admin')->middleware(['CheckAdmin'])->group(function () {

        Route::get('empleado','EmpleadoController@index')->name('empleado.index');
        Route::get('empleado/create','EmpleadoController@create')->name('empleado.create');
        Route::post('empleado/store','EmpleadoController@store')->name('empleado.store');
        Route::post('empleado/update','EmpleadoController@update')->name('empleado.update');
        Route::get('empleado/edit/{id}','EmpleadoController@edit')->name('empleado.edit');
        Route::get('empleado/buscar','EmpleadoController@buscar')->name('empleado.search');

        Route::get('proveedor','ProveedoresController@index')->name('proveedor.index');
        Route::get('proveedor/buscar','ProveedoresController@buscar')->name('proveedor.search');
        Route::get('proveedor/create','ProveedoresController@create')->name('proveedor.create');
        Route::post('proveedor/store','ProveedoresController@store')->name('proveedor.store');
        Route::post('proveedor/update','ProveedoresController@update')->name('proveedor.update');
        Route::post('proveedor/import','ProveedoresController@importUser')->name('proveedor.import');
        Route::get('proveedor/edit/{id}','ProveedoresController@edit')->name('proveedor.edit');
        Route::get('proveedor/delete/{id}','ProveedoresController@delete')->name('proveedor.delete');

        Route::get('mercancia','MercanciaController@index')->name('mercancia.index');
        Route::get('mercancia/buscar','MercanciaController@buscar')->name('mercancia.search');
        Route::get('mercancia/create','MercanciaController@create')->name('mercancia.create');
        Route::post('mercancia/store','MercanciaController@store')->name('mercancia.store');
        Route::post('mercancia/update','MercanciaController@update')->name('mercancia.update');
        Route::post('mercancia/import','MercanciaController@importUser')->name('mercancia.import');
        Route::get('mercancia/edit/{id}','MercanciaController@edit')->name('mercancia.edit');
        Route::get('mercancia/delete/{id}','MercanciaController@delete')->name('mercancia.delete');



        Route::get('inventario','InventarioController@index')->name('inventario.index');
        Route::get('inventario/buscar','InventarioController@buscar')->name('inventario.search');
         Route::get('inventario/reporte','InventarioController@reporte')->name('inventario.reporte');



    });


      Route::prefix('super')->middleware(['CheckSuper'])->group(function () {
        Route::get('empresa','EmpresaController@index')->name('empresa.index');
        Route::get('users','UsuariosController@index')->name('users.index');
        Route::get('users/buscar','UsuariosController@buscar')->name('users.search');
        Route::get('users/create','UsuariosController@create')->name('users.create');
        Route::post('users/store','UsuariosController@store')->name('users.store');
        Route::post('users/update','UsuariosController@update')->name('users.update');
        Route::post('users/import','UsuariosController@importUser')->name('users.import');
        Route::get('users/edit/{id}','UsuariosController@edit')->name('users.edit');
        Route::get('users/delete/{id}','UsuariosController@delete')->name('users.delete');
        Route::get('reports','Reports@index')->name('reports.index');
        

    });



});

