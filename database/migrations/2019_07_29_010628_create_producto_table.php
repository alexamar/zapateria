<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

         Schema::create('producto_color', function (Blueprint $table) {
               $table->increments('id');
            $table->integer('color_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->foreign('color_id')->references('id')->on('color');
            $table->foreign('producto_id')->references('id')->on('producto');
            $table->timestamps();
           
        });

       Schema::create('producto_talla', function (Blueprint $table) {
         $table->integer('producto_color_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_color_id')->references('id')->on('producto_color');
            $table->foreign('producto_id')->references('id')->on('producto');
            $table->timestamps();
           
        });


          Schema::create('producto_proveedor', function (Blueprint $table) {
            $table->integer('proveedor_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->foreign('proveedor_id')->references('id')->on('proveedor');
            $table->foreign('producto_id')->references('id')->on('producto');
            $table->timestamps();
           
        });

        Schema::create('producto_categoria', function (Blueprint $table) {
            $table->integer('categoria_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categoria');
            $table->foreign('producto_id')->references('id')->on('producto');
            $table->timestamps();
           
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto');
    }
}
