<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->admin()){
             $type=Auth::user()->tipo;

           if ($type == '2'){
                       return $next($request);
           }
           if ($type == '3'){
                    return redirect()->route('Administración');
           }
     
        }
        return redirect()->route('Administración');
    }
}
