<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

  /* $users = DB::table('users')->where([
    ['status', '=', '1'],
    ['subscribed', '<>', '1'],
    ])->get();*/

     $users = DB::table('users')->where('tipo', 2)->orWhere('tipo', 1)->paginate(20);
     return view('super.users.index', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       

        //echo ("ENTRE POR ACA");
        return view('super.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'contraseña' => 'required|string|min:6',
                ]);

        if ($validator->fails()) {
             $error = $validator->errors()->first();
        
            return redirect('super/users/create')->withErrors($validator)->withInput();
        }
        
   


       $date = date("Y-n-j", time());
        DB::table('users')->insert(['name' => $request->get('nombre'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('contraseña')),
            'tipo' => $request->get('rol'),
            'updated_at' => $date,
            'confirmed' => 1,
            'created_at' => $date,
            'empleado_id' => $request->get('empleado')
        ]);

        session()->flash('info', ['title' => 'Registro Exitoso', 'text' => 'Usuario Registrado', 'type' => 'success']);
        return redirect()->route('users.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

     public function delete(Request $request)
    {

       
        DB::table('users')->where('id', $request->route('id'))->delete();
        session()->flash('info', ['title' => ' Operación realizada', 'text' => 'Usuario Eliminado', 'type' => 'info']);
        return redirect()->route('users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        $user = DB::table('users')->where('id', $request->route('id'))->first();
   
        return view('super.users.edit', compact(['user']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
        ]);

            $validator->sometimes('password', 'required|string|min:6', function($input) {
                return $input->password <> null;
            });
             
          $validator->sometimes('email', 'required|string|email|max:255|unique:users', function($input) {
            $email = DB::table('users')->where('id', '=', $input->id)->first()->email;
                return $input->email <> $email;
            });

        if ($validator->fails()) {
             $error = $validator->errors()->first();
            $valor=$request->get('id');
             return redirect('super/users/edit/'.$valor)->withErrors($validator)->withInput();
        }

        $date = date("Y-n-j", time());
           
        if ($request->get('contraseña')) {
            DB::table('users')->where('id', '=', $request->get('id'))->update([
            'name' => $request->get('nombre'),
            'email' => $request->get('email'),
            'tipo' => $request->get('rol'),
            'password' => Hash::make($request->get('contraseña')),
            'updated_at' => $date,
             'empleado_id' => $request->get('empleado')
            ]);
        } else {
            DB::table('users')->where('id', '=', $request->get('id'))->update([
            'name' => $request->get('nombre'),
            'email' => $request->get('email'),
            'tipo' => $request->get('rol'),
            'created_at' => $date,
            'empleado_id' => $request->get('empleado')
            ]);
        }
        
        session()->flash('info', ['title' => 'Registro Exitoso', 'text' => 'Usuario Actualizado', 'type' => 'success']);
        return redirect()->route('users.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
