<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Countries\Package\Countries;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class MercanciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
               $data = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                 ->select('producto.descripcion','producto_id' ,'color.nombre','talla', DB::raw('count(*) as total'),'producto.precio')
                 ->groupBy('producto.descripcion','producto_id','color.nombre','talla','producto.precio')
                 ->paginate(20);

               return view('admin.mercancia.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.mercancia.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
      /*  echo $request->proveedor;
        echo "<br>";
        echo $request->categoria;
        echo "<br>";
        echo $request->color;
        echo "<br>";
        echo $request->precio;
        echo "<br>";
        echo $request->talla;
        echo "<br>";
        echo $request->cantidad;
        echo "<br>";
        echo $request->descripcion;
        echo "<br>";*/

        $date = date("Y-n-j", time());

        DB::table('producto')->insert(['descripcion' => $request->get('descripcion'),
            'updated_at' => $date,
            'created_at' => $date,
            'precio' => $request->get('precio')
        ]);

        $aux =  DB::table('producto')->orderBy('id', 'desc')->first();

        for($i= 0; $i < $request->cantidad; $i++ ){

        DB::table('producto_categoria')->insert(['categoria_id' => $request->get('categoria'),
            'producto_id' => $aux->id,
            'updated_at' => $date,
            'created_at' => $date
        ]);

         DB::table('producto_proveedor')->insert(['proveedor_id' => $request->get('proveedor'),
            'producto_id' => $aux->id,
            'updated_at' => $date,
            'created_at' => $date
        ]);

        DB::table('producto_color')->insert(['color_id' => $request->get('color'),
            'producto_id' => $aux->id,
            'talla' =>  $request->get('talla'),
            'updated_at' => $date,
            'created_at' => $date
        ]);

        }

        
        session()->flash('info', ['title' => 'Registro Exitoso', 'text' => 'Mercancia Registrada', 'type' => 'success']);
        return redirect()->route('mercancia.index');



    }

     public function delete(Request $request)
    {


        echo $request->route('id');
       
        DB::table('producto_proveedor')->where('producto_id', $request->route('id'))->delete();
        DB::table('producto_color')->where('producto_id', $request->route('id'))->delete();
        DB::table('producto_categoria')->where('producto_id', $request->route('id'))->delete();
        DB::table('producto')->where('id', $request->route('id'))->delete();

        session()->flash('info', ['title' => ' Operación realizada', 'text' => 'Mercancia Eliminada', 'type' => 'info']);
       

       return redirect()->route('mercancia.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
       
         $producto = DB::table('producto')->where('id', $request->route('id'))->first();
         $producto_categoria = DB::table('producto_categoria')->where('producto_id', $request->route('id'))->first();
         $producto_color = DB::table('producto_color')->where('producto_id', $request->route('id'))->first();
         $producto_proveedor = DB::table('producto_proveedor')->where('producto_id', $request->route('id'))->first();

        //Sacando la cantidad
        $data = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                 ->select( DB::raw('count(*) as total'))
                 ->groupBy('producto.descripcion','producto_id','color.nombre','talla','producto.precio')
                 ->where('producto_id', $request->route('id'))->first();

         $total = $data->total;       


         return view('admin.mercancia.edit', compact(['producto','producto_categoria','producto_color','producto_proveedor','total']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        //Aqui deberian ir las validaciones xd

         $date = date("Y-n-j", time());


         DB::table('producto')->where('id', '=', $request->get('id'))->update([
            'descripcion' => $request->get('descripcion'),
            'updated_at' => $date,
            'precio' => $request->get('precio')
            ]);

        //Sacando la cantidad
        $data = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                 ->select( DB::raw('count(*) as total'))
                 ->groupBy('producto.descripcion','producto_id','color.nombre','talla','producto.precio')
                 ->where('producto_id', $request->id)->first();

         $total = $data->total;      

         if($total==$request->cantidad){ // If de no aumentar nada
         
        DB::table('producto_categoria')->where('producto_id', '=', $request->get('id'))->update([
            'categoria_id' => $request->get('categoria'),
            'updated_at' => $date,
            
            ]);

         DB::table('producto_color')->where('producto_id', '=', $request->get('id'))->update([
            'color_id' => $request->get('color'),
            'talla' => $request->get('talla'),
            'updated_at' => $date
            
            ]);

        DB::table('producto_proveedor')->where('producto_id', '=', $request->get('id'))->update([
            'proveedor_id' => $request->get('proveedor'),
            'updated_at' => $date,
            ]);

         }
         else if($total<$request->cantidad){ // if de disminuir registros, hacerlo en otro momento xd
         
         $var = $request->cantidad - $total;

         // actualizo todos los que hay
        DB::table('producto_categoria')->where('producto_id', '=', $request->get('id'))->update([
            'categoria_id' => $request->get('categoria'),
            'updated_at' => $date,
            
            ]);

         DB::table('producto_color')->where('producto_id', '=', $request->get('id'))->update([
            'color_id' => $request->get('color'),
            'talla' => $request->get('talla'),
            'updated_at' => $date
            
            ]);

        DB::table('producto_proveedor')->where('producto_id', '=', $request->get('id'))->update([
            'proveedor_id' => $request->get('proveedor'),
            'updated_at' => $date,
            ]);

        // creo los nuevos
        for($i = 0; $i < $var ; $i++ ){
         
        DB::table('producto_categoria')->insert(['categoria_id' => $request->get('categoria'),
            'producto_id' => $request->get('id'),
            'updated_at' => $date,
            'created_at' => $date
        ]);

         DB::table('producto_proveedor')->insert(['proveedor_id' => $request->get('proveedor'),
            'producto_id' => $request->get('id'),
            'updated_at' => $date,
            'created_at' => $date
        ]);

        DB::table('producto_color')->insert(['color_id' => $request->get('color'),
            'producto_id' => $request->get('id'),
            'talla' =>  $request->get('talla'),
            'updated_at' => $date,
            'created_at' => $date
        ]);  

        }



         }
         else{
            echo "entro al menor";
         }


         // retornar a la vista

        session()->flash('info', ['title' => 'Registro Exitoso', 'text' => 'Mercancia Actualizada', 'type' => 'success']);
        return redirect()->route('mercancia.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
