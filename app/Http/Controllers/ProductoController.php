<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Countries\Package\Countries;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\producto;


class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       /* $user = producto::find(1);
        foreach ($user->categorias as $c) {
            dd($c->nombre);
           }
*/
            
         $data = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                 ->select('producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto.descripcion','producto_id','color.nombre','talla')
                 ->paginate(20);
               return view('user.producto.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function buscar(Request $request)
    {
         $pc = DB::table('producto')
                    ->join('producto_categoria', 'producto.id', '=', 'producto_id')
                    ->join('categoria', 'categoria.id', '=', 'categoria_id')
                    ->Where('categoria.nombre', 'LIKE', "%" . $request->get('producto') . "%")
                    ->pluck("producto.id"); 

            $pp = DB::table('producto')
                    ->join('producto_proveedor', 'producto.id', '=', 'producto_id')
                    ->join('proveedor', 'proveedor.id', '=', 'proveedor_id')
                    ->Where('proveedor.nombre', 'LIKE', "%" . $request->get('producto') . "%")
                    ->pluck("producto.id"); 
      
        

               $data = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                 ->select('producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto.descripcion','producto_id','color.nombre','talla')
                 ->Where('producto.descripcion', 'LIKE', "%" . $request->get('producto') . "%")
                 ->orWhere('color.nombre', 'LIKE', "%" . $request->get('producto') . "%")
                 ->orWhere('talla', 'LIKE', "%" . $request->get('producto') . "%")
                 ->orWhereIn('producto_id', $pc )
                 ->orWhereIn('producto_id', $pp )
                 ->paginate(20);
                     return view('user.producto.index', compact('data'));

                
    }


}
