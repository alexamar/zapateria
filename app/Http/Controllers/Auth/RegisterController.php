<?php

namespace App\Http\Controllers\Auth;

use Mail;
use App\User;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use PragmaRX\Countries\Package\Countries;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

   public function showRegistrationForm()
    {
        $departamentos=DB::table('departamentos')->get();
 
              $departamentos = $departamentos->pluck('departamento','id_departamento')->sort();
   
        return view("auth.register", compact("departamentos"));
     }

      public function GetMunicipios(Request $request,$id)
    {
        if($request->ajax()){

                $mis_municipios=DB::table('municipios')->where('departamento_id',$id)->get();
                return response()->json($mis_municipios);

        }
  
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
     protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
     protected function create(array $data)
    {
        
      
        $confirmation_code=str_random(25);

        $user=new User();
        $user->name=$data['name'];
        $user->email= $data['email'];
        $user->municipio_id= $data['municipio'];
        $user->password=Hash::make($data['password']);
        $user->tipo=2;
        $user->confirmation_code=$confirmation_code;
        $user->save();
    
        //dd(__('message.send'));
        Mail::send('emails.confirmation_code',  ['user' => $user], function($message) use ($user) {
        $message->to($user->email,  $user->name)->subject(__('message.confirmedemail'));
        });

       return $user;
       // Session::flash('info', ['title' => 'Registro Exitoso', 'text' => 'Confirma tu correo para poder ingresar', 'type' => 'info']);
       //return redirect('login');
    }

 public function register(Request $request)
    {
       $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
       // $this->guard()->login($user);
        Session::flash('info', ['title' => __('message.reg_success'), 'text' => __('message.reg_text'), 'type' => 'info']);
        return $this->registered($request, $user)
            ?: redirect('login');
    }

    public function verify($code)
    {
    $user = User::where('confirmation_code', $code)->first();

    if (! $user)
        return redirect('/');

    $user->confirmed = true;
    $user->confirmation_code = null;
    $user->save();
    Auth::login($user, true);
    Session::flash('info', ['title' => '', 'text' =>  __('message.conf_text'), 'type' => 'success']);
         return redirect('/home');
    }
}
