<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       if (Auth::user()->activo== '1'){
          $type=Auth::user()->tipo;
            if ($type == '3'){
               return $this->super();
           }
           if ($type == '1'){
               return $this->admin();
           }else if($type == '2'){
               return $this->alumn();
           }
       }else{
           Auth::logout();
           session()->flash('info',['title'=>'Cuenta Inactiva','text'=>'Su cuenta se encuentra inactiva, contacte a su empleador.','type'=>'warning']);
           return redirect('/login');
       }
    }

    public function alumn(){
     return view('user.home');
    }

    public function super(){
      
     return view('super.home');
    }


    public function admin()
    {
     

        return view('admin.home');
    }

    public function getUsers($eval){
        $curso=DB::table('curso')->where('id',$eval->id_curso)->first();
        $contratos=json_decode($curso->id_contrato);
        $sumatoriaUser=0;
        foreach ($contratos as $contrato){
            $cant=DB::table('users')->where('id_contrato',$contrato)->count();
            $sumatoriaUser+=$cant;
        }
        return $sumatoriaUser;
    }

    public function respuestas($eval){
        $curso=DB::table('curso')->where('id',$eval->id_curso)->first();
        $result=DB::table('resultado')->where('curso_id',$curso->id)->get();
        $suma=0;
        foreach ($result as $re){
            $suma+=$re->calificacion;
        }
        $respuestas=count($result);
        $promedio=$suma/$respuestas;
        return ["r"=>$respuestas,"p"=>$promedio];
    }
}
