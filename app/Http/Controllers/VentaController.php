<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Countries\Package\Countries;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\producto;
use Illuminate\Support\Facades\Auth;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       

                // dd($productos);

                if ($request->session()->has('cart')) {
                     $data = $request->session()->get('cart');

                     $array_p = [];

                  foreach ($data as $d) {


                    $prod= DB::table('producto_color')
                            ->join('color', 'color.id', '=', 'color_id')
                            ->join('producto', 'producto.id', '=', 'producto_id')
                      ->select('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                         ->groupBy('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                         ->where('producto_id',$d->producto_id)
                         ->where('producto.descripcion', $d->descripcion)
                         ->where('color.nombre', $d->nombre)
                         ->where('talla', $d->talla)
                         ->get(); 


                         foreach ( $prod as $p) { 

                            array_push($array_p, $p->id);
                         }

                    

                    }

                     $productos = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                     ->whereNotIn('producto_color.id', $array_p)
                 ->select('producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                 ->get();


    //
                }else{
                        $data = [];
                        
                           $productos = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                 ->select('producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                 ->get();

  

                }



               return view('user.venta.index',  compact('data','productos'));
    }

    public function GetCliente(Request $request,$id)
    {
      if($request->ajax()){

        $cliente=DB::table('cliente')->where('cedula',$id)->get()->first();
        return response()->json($cliente);

      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $userid= Auth::user()->id;
      DB::table('factura_cliente')->insert([
            'cliente_cedula' => $request->get('cedula2'),
            'cajero_id' => $userid,
            'empresa_id' => 1,
            'tipo_pago' => $request->get('tipopago')
        ]);

      $data = $request->session()->get('cart');
      $cantidad = $request->get('cantidad');
      $cont = 0;
      $f_id =  DB::table('factura_cliente')->find(DB::table('factura_cliente')->max('id'));
      foreach ($data as $d) {


            $producto= DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
              ->select('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                 ->where('producto_id',$d->producto_id)
                 ->where('producto.descripcion', $d->descripcion)
                 ->where('color.nombre', $d->nombre)
                 ->where('talla', $d->talla)
                 ->get()->first(); 

                

              DB::table('factura_detalle')->insert([
                    'factura_id' => $f_id->id,
                    'producto_id' =>  $d->producto_id,
                    'color' =>   $d->nombre,
                    'talla' =>  $d->talla,
                    'cantidad' => $cantidad[$cont]
              
                ]);

              $pr= DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
              ->select('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                 ->where('producto_id',$d->producto_id)
                 ->where('producto.descripcion', $d->descripcion)
                 ->where('color.nombre', $d->nombre)
                 ->where('talla', $d->talla)
                 ->pluck("id"); 

    

                       for($i=0; $i< $cantidad[$cont]; $i++){
                          DB::table('producto_color')->where('id', $pr[$i])->delete();
                        }


                 // DB::table('producto_color')->where('id', $request->route('id'))->delete();

            $cont++;
      }

       $data = $request->session()->forget('cart');
      
session()->flash('info', ['title' => 'Operacion Exitosa', 'text' => 'Factura Procesada', 'type' => 'success']);
    return redirect()->route( 'ver.factura' );

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verFactura(Request $request)
    {
         $f_id =  DB::table('factura_cliente')->find(DB::table('factura_cliente')->max('id'));
            $factura= DB::table('factura_cliente')->where('id', $f_id->id)->get()->first(); 
           $productos= DB::table('factura_detalle')->where('factura_id', $f_id->id)->get(); 

               return view('user.venta.factura',  compact('factura','productos'));
 

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function delete(Request $request)
    {
       //dd($request->route('id'));
        $cart = $request->session()->get('cart');
        unset($cart[$request->route('id')]);
        $cart = array_values($cart); 
     
        $request->session()->put('cart', $cart);
      // $request->session()->forget('cart');

          if ($request->session()->has('cart')) {
                     $data = $request->session()->get('cart');

                     $array_p = [];

                  foreach ($data as $d) {


                    $prod= DB::table('producto_color')
                            ->join('color', 'color.id', '=', 'color_id')
                            ->join('producto', 'producto.id', '=', 'producto_id')
                      ->select('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                         ->groupBy('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                         ->where('producto_id',$d->producto_id)
                         ->where('producto.descripcion', $d->descripcion)
                         ->where('color.nombre', $d->nombre)
                         ->where('talla', $d->talla)
                         ->get(); 


                         foreach ( $prod as $p) { 

                            array_push($array_p, $p->id);
                         }

                    

                    }

                     $productos = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                     ->whereNotIn('producto_color.id', $array_p)
                 ->select('producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                 ->get();


    //
                }else{
                        $data = [];
                        
                           $productos = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                 ->select('producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                 ->get();

  

                }


               return view('user.venta.index',  compact('data','productos'));
      
    }

     public function agregar(Request $request)
    {
        //dd($request->get('producto'));
        $array = explode("+", $request->get('producto'));
        //$array[0]
   
            $producto= DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                 ->select('producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                 ->where('producto_id', $array[0])
                 ->where('producto.descripcion', $array[1])
                 ->where('color.nombre', $array[2])
                 ->where('talla', $array[3])
                 ->get()->first(); 

                 if ($request->session()->has('cart')) {
                    $request->session()->push('cart', $producto);
    //
                }else{
                       $request->session()->push('cart', $producto);
                            
  

                }

       

         
         $data = $request->session()->get('cart');
   if ($request->session()->has('cart')) {
                     $data = $request->session()->get('cart');

                     $array_p = [];

                  foreach ($data as $d) {


                    $prod= DB::table('producto_color')
                            ->join('color', 'color.id', '=', 'color_id')
                            ->join('producto', 'producto.id', '=', 'producto_id')
                      ->select('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                         ->groupBy('producto_color.id','producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                         ->where('producto_id',$d->producto_id)
                         ->where('producto.descripcion', $d->descripcion)
                         ->where('color.nombre', $d->nombre)
                         ->where('talla', $d->talla)
                         ->get(); 


                         foreach ( $prod as $p) { 

                            array_push($array_p, $p->id);
                         }

                    

                    }

                     $productos = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                     ->whereNotIn('producto_color.id', $array_p)
                 ->select('producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                 ->get();


    //
                }else{
                        $data = [];
                        
                           $productos = DB::table('producto_color')
                    ->join('color', 'color.id', '=', 'color_id')
                    ->join('producto', 'producto.id', '=', 'producto_id')
                 ->select('producto.precio','producto.descripcion','producto_id','color.nombre','talla', DB::raw('count(*) as total'))
                 ->groupBy('producto.precio','producto.descripcion','producto_id','color.nombre','talla')
                 ->get();

  

                }
         
               return view('user.venta.index', compact('data','productos'));

    }
}
