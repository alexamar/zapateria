<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Countries\Package\Countries;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ProveedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $proveedores = DB::table('proveedor')->paginate(20);
         return view('admin.proveedores.index', compact('proveedores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

      //  echo "TODO BIEN";
        return view('admin.proveedores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
             $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'numero_identificacion' => 'required|string|max:255|unique:proveedor',
            'descripcion' => 'required|string|max:400',
                ]);


        if ($validator->fails()) {
             $error = $validator->errors()->first();
           
            return redirect('admin/proveedor/create')->withErrors($validator)->withInput();
        }

       $date = date("Y-n-j", time());

       DB::table('proveedor')->insert(['nombre' => $request->get('nombre'),
            'numero_identificacion' => $request->get('numero_identificacion'),
            'descripcion' => $request->get('descripcion'),
            'updated_at' => $date,
            'created_at' => $date
        ]);

       session()->flash('info', ['title' => 'Registro Exitoso', 'text' => 'Proveedor Registrado', 'type' => 'success']);
        return redirect()->route('proveedor.index');
      
    }

     public function delete(Request $request)
    {

       
        DB::table('proveedor')->where('id', $request->route('id'))->delete();
        session()->flash('info', ['title' => ' Operación realizada', 'text' => 'Proveedor Eliminado', 'type' => 'info']);
        return redirect()->route('proveedor.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
       // echo "ENTRE POR ACA";
        
        $proveedor = DB::table('proveedor')->where('id', $request->route('id'))->first();
        return view('admin.proveedores.edit', compact(['proveedor']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function update(Request $request)
    {
        
       
       // Aqui deberian ir las validaciones xd

        $date = date("Y-n-j", time());
           
    


            DB::table('proveedor')->where('id', '=', $request->get('id'))->update([
            'nombre' => $request->get('nombre'),
            'numero_identificacion' => $request->get('numero_identificacion'),
            'descripcion' => $request->get('descripcion'),
            'updated_at' => $date,
            
            ]);
         


        session()->flash('info', ['title' => 'Registro Exitoso', 'text' => 'Proveedor Actualizado', 'type' => 'success']);
        return redirect()->route('proveedor.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
