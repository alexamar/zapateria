<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Countries\Package\Countries;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\cierre;
use Carbon\Carbon;

class CierreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = Carbon::now('America/Caracas')->format('Y-m-d');
        $sql = DB::table('factura_cliente')
                    ->join('users', 'users.id','=','factura_cliente.cajero_id')
                    ->join('empleado', 'empleado.id','=','users.empleado_id')
                    ->join('cliente','cliente.cedula','=','cliente_cedula')
                    ->join('factura_detalle','factura_detalle.factura_id','=','factura_cliente.id')
                    ->join('producto','producto.id','=','factura_detalle.producto_id')
                ->select('factura_cliente.fecha','empleado.nombre as empleado','cliente.nombre','producto.descripcion','factura_detalle.cantidad','producto.precio')
                ->whereDate('fecha',$now)
                //->whereDate('fecha','2019-09-22')
                ->paginate(20);
                //dd($sql);
                return view('user.cierre.index', compact('sql'));
    }

    /* SELECT tabla1.fecha AS "fecha",tabla3.nombre AS "Cajero" ,
     tabla4.nombre AS "Cliente", tabla6.descripcion as "Producto",
      tabla5.cantidad AS "Cantidad", tabla6.precio as "Precio"
       FROM factura_cliente AS tabla1
        INNER JOIN users AS tabla2
        ON (tabla2.id = tabla1.cajero_id)
        INNER JOIN empleado AS tabla3
        ON (tabla2.empleado_id = tabla3.id)
        INNER JOIN cliente AS tabla4
        ON (tabla1.cliente_cedula = tabla4.cedula)
        INNER JOIN factura_detalle AS tabla5
        ON (tabla5.factura_id = tabla1.id)
        INNER JOIN producto AS tabla6
        ON (tabla5.producto_id = tabla6.id)
            WHERE tabla1.fecha = '2019-09-22 19:46:17';
/*

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
