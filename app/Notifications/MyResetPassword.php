<?php
 
namespace App\Notifications;
 
use Illuminate\Support\Facades\Lang; 
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
 
class MyResetPassword extends ResetPassword
{
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject(__('message.resetpassword_1'))
            ->line(__('message.resetpassword_2'))
            ->action(__('message.resetpassword_3'), url('/password/reset/' . $this->token))
            ->line(__('message.resetpassword_4'));
   
    }
}

