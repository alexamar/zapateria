<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
    //
    protected $table = 'producto';
     public $timestamps = false;

      public function categorias()
    {
        return $this->belongsToMany('App\categoria', 'producto_categoria');
    }
       public function proveedores()
    {
        return $this->belongsToMany('App\proveedor', 'producto_proveedor');
    }
}
