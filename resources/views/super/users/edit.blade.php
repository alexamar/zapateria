@extends('super.template')
@php
$empleados = DB::table('empleado')->get();
@endphp
@section('contenido')
    <div class="row my-4">
        <div class="col-md-6">
            <h2><b>EDICIÓN USUARIO</b></h2>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-md-12">
            <form class="row" method="POST" action="{{ route('users.update') }}">
                {{ csrf_field() }}
                <input hidden name="id" value="{{ $user->id }}">
                               <div class="form-group row col-6">

                    <label for="nombre" class="col-sm-5 col-form-label">Nombre:</label>
                    <div class="col-sm-10">
                        <input   value="{{$user->name}}" type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                    </div>
                </div>

                
                    <div class="form-group row col-6">
                 
                    <label for="email" class="col-sm-5 col-form-label">Email:</label>
                    <div class="col-sm-10">
                        <input   value="{{$user->email}}" type="text" class="form-control" id="email" name="email" placeholder="Email" required>
                         @if ($errors->has('email'))
                                        <span class="help-block">
                                     {{ $errors->first('email') }}
                                    </span>
                                    @endif
                    </div>
                  
                </div>
                       
                <div class="form-group row col-6">
                    <label for="password" class="col-sm-5 col-form-label">Contraseña:</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="contraseña" name="contraseña" placeholder="Contraseña" >
                         @if ($errors->has('contraseña'))
                                        <span class="help-block">
                                      {{ $errors->first('contraseña') }}
                                    </span>
                                    @endif
                    </div>
                </div>
                <div class="form-group row col-6">
                    <label for="rol" class="col-sm-10 col-form-label">Rol:</label>
                    <div class="col-sm-10">
                        <select class="custom-select" id="rol" name="rol" required>
                            <option value="2" selected>Cajero</option>
                            <option value="1">Administrador</option>
                        </select>
                    </div>
                </div>

                 <div class="form-group row col-6">
                    <label for="rol" class="col-sm-10 col-form-label">Empleado:</label>
                    <div class="col-sm-10">
                        <select class="custom-select" id="empleado" name="empleado" required>
                          @foreach($empleados as $emp)

                            @if ($user->empleado_id == $emp->id)
                            <option value="{{$emp->id}}" selected="true">{{$emp->nombre}}</option>
                            @else
                            <option value="{{$emp->id}}">{{$emp->nombre}}</option>
                            @endif
                           
                          @endforeach   
                        </select>
                    </div>
                </div>
                
                <div class="form-group row col-11 justify-content-center">
                    <button type="submit" class="btn btn-primary bt-md">Actualizar</button>
                </div>
            </form>
        </div>
    </div>

         <script>
        
$(document).ready(function(){ 
    $("#departamento").val("");
$("#departamento").on('change',function(event){

$.get("/efecticarga/public/admin/municipios/"+event.target.value+"", function(response, state){
    console.log(event.target.value);
    $("#municipio").empty();
    for(i=0;i<response.length;i++){
      $("#municipio").append("<option value='"+response[i].id_municipio+"'>"+response[i].municipio+"</option>");
    }
        });

    });

});﻿




    </script>
@endsection