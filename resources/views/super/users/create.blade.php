@extends('super.template')

@php
$empleados = DB::table('empleado')->get();
@endphp

@section('contenido')
    <div class="row my-4">
        <div class="col-md-9">
            <h2  style="margin-left: 50%">CREACIÓN NUEVO USUARIO</h2>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-md-12">
            <form class="row" method="POST" action="{{ route('users.store') }}">
                {{ csrf_field() }}
                <div class="form-group row col-6">

                    <label for="nombre" class="col-sm-5 col-form-label">Nombre:</label>
                    <div class="col-sm-10">
                        <input   value="{{old('nombre')}}" type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                    </div>
                </div>
                  
                                <div class="form-group row col-6">
                 
                    <label for="email" class="col-sm-5 col-form-label">Email:</label>
                    <div class="col-sm-10">
                        <input   value="{{old('email')}}" type="text" class="form-control" id="email" name="email" placeholder="Email" required>
                         @if ($errors->has('email'))
                                        <span class="help-block">
                                     {{ $errors->first('email') }}
                                    </span>
                                    @endif
                    </div>
                  
                </div>
                       
                <div class="form-group row col-6">
                    <label for="password" class="col-sm-5 col-form-label">Contraseña:</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="contraseña" name="contraseña" placeholder="Contraseña" required>
                         @if ($errors->has('contraseña'))
                                        <span class="help-block">
                                      {{ $errors->first('contraseña') }}
                                    </span>
                                    @endif
                    </div>
                </div>
                <div class="form-group row col-6">
                    <label for="rol" class="col-sm-10 col-form-label">Rol:</label>
                    <div class="col-sm-10">
                        <select class="custom-select" id="rol" name="rol" required>
                            <option value="2" selected>Cajero</option>
                            <option value="1">Administrador</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row col-6">
                    <label for="rol" class="col-sm-10 col-form-label">Empleado:</label>
                    <div class="col-sm-10">
                        <select class="custom-select" id="empleado" name="empleado" required>
                          @foreach($empleados as $emp)
                           <option value="{{$emp->id}}">{{$emp->nombre}}</option>
                          @endforeach   
                        </select>
                    </div>
                </div>
                <div class="form-group row col-11 justify-content-center">
                    <button type="submit" class="btn btn-primary bt-md">Registrar</button>
                </div>
            </form>
        </div>
    </div>
    
    <script>
        


    </script>
@endsection