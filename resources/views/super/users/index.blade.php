@extends('super.template')
@section('contenido')
    <div class="row my-4">
        <div class="col-md-9">
           <h1 style="margin-left: 50%">LISTA DE USUARIOS</h1>
        </div>
        <div class="col-md-2">
            <a class="btn btn-primary btn-icon" href="{{ route('users.create') }}">
                <i class="material-icons">add</i>
                Crear Usuario
            </a>
        </div>
    </div>
    <div class="row my-3">
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            <form class="form-inline" type="POST" action="{{ route('users.search') }}">
                <label class="sr-only" for="inlineFormInput">Nombre</label>
                <input type="text"class="form-control mb-2 mr-sm-2 mb-sm-0" name="usuario" id="usuario" placeholder="Nombre o Correo" required>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </form>
        </div>
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Activo</th>
                    <th scope="col">Empleado</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $us)
                    @php 

                      $empleados = DB::table('empleado')->where('id',$us->empleado_id)->first();
                     
                       @endphp
                        <tr>
                            <td>{{ $us->name }}</td>
                            <td>{{ $us->email }}</td>
                            <td>{{ $us->tipo }}</td>
                            <td>{{ ($us->activo == '1')?'Activo':'Inactivo' }}</td>
                            <td>{{ $empleados->nombre }}</td>
                            <td class="btn-action">
                                <a class="btn btn-warning btn-icon" href="{{ route('users.edit',$us->id) }}">
                                    <i class="material-icons">mode_edit</i>
                                    Editar
                                </a>
                                <a class="btn btn-danger btn-icon" href="{{ route('users.delete',$us->id) }}">
                                    <i class="material-icons">delete</i>
                                    Eliminar
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            
        </div>
    </div>

@endsection
