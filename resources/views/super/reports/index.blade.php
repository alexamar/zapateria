@extends('super.template')
@section('contenido')
    <div class="row my-4">
        <div class="col-md-9">
            <h1><b>REPORTES</b></h1>
        </div>
    </div>
    <div class="row my-3">
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            <form class="form-inline" type="POST" action="{{ route('users.search') }}">
                <label class="sr-only" for="inlineFormInput">Fecha</label>
                <input type="text"class="form-control mb-2 mr-sm-2 mb-sm-0" name="usuario" id="usuario" placeholder="Nombre o Correo" required>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </form>
        </div>
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Fecha</th>
                    <th scope="col">N° de Factura</th>
                    <th scope="col">Nombre Cliente</th>   
                    <th scope="col">Cajero</th>                 
                    <th scope="col">Empresa</th>
                    <th scope="col">Pago</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($reports as $item)
                    @php 

                      
                     
                       @endphp
                        <tr>
                            <td>{{ $item->fecha }}</td>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->cliente_n }}</td>
                            <td>{{ $item->cajero }}</td>                                                        
                            <td>{{ $item->empresa }}</td>
                            <td>{{ $item->pago }}</td>                            
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            
        </div>
    </div>

@endsection
