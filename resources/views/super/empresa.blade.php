@extends('super.template')
@section('contenido')
    <div class="justify-content-center my-4" style="display: flex;width: 100%;">
              <h1>DATOS DE LA EMPRESA</h1>
  
       
    </div>
 
    <div class="datos">
          <a alt="Inicio"  class="icon2">  <img  src="{{ asset('img/icon.png') }}">  </a> 
        <div class="col-md-6">
      <form">
  <div class="form-group">
  <div class="justify-content-center" style="display: flex;width: 100%;">  <label for="exampleInputEmail1">Nombre de la Empresa</label>  </div>
    <input type="text" value="{{ $valores->nombre }}" class="form-control" id="exampleInputEmail1">

  </div>
  <div class="form-group">
   <div class="justify-content-center" style="display: flex;width: 100%;"> <label for="exampleInputPassword1">Numero de Registro de Información Fiscal (RIF)</label></div>
    <input type="number"  value="{{ $valores->registro }}" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
   <div class="form-group">
   <div class="justify-content-center" style="display: flex;width: 100%;"> <label for="exampleInputPassword1">Direccion de la Empresa</label></div>
     <textarea class="form-control" name="respuesta" id="respuesta" rows="5">{{ $valores->direccion}}</textarea>
  </div>
      <div class="justify-content-center my-4" style="display: flex;width: 100%;"><button type="submit" class="btn btn-success">Actualizar Datos</button></div>
</form>
        </div>
      
    </div>

@endsection
