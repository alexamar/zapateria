@extends('layouts.user')

@section('content')




    <div class="row">
            <div class="col">
        <div class="fondo"></div></div>
        <div class="col">
            <div class="panel panel-default">
       
   

                       <div class="row justify-content-center"> 
  <a alt="Inicio"  class="icon">
                    <img  src="{{ asset('img/icon.png') }}">
                </a>  </div>
                <div class="panel-body">
                
                    <form class="form-horizontal col-md-10" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                     <div class="form-group">   <div class="row justify-content-center">    <label>Bienvenido al sistema</label>   </div>    </div>
                            <div class="row justify-content-center">             
                        <div class="form-group">
                            <div class="row justify-content-center"> <label>Usuario</label>   </div>

                            <div class="col-md-12">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            </div>
                        </div>

                        <div class="form-group">
                           <div class="row justify-content-center"> <label>Contraseña</label>   </div>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                            </div>
                        </div> </div> 
                         <div class="row justify-content-center">
                            <div class="form-group">
                            
                                <button type="submit" class="btn btn-primary btn-block">
                                    Iniciar Sesion
                                </button>
                            </div>
                             
                             @if ($errors->has('email'))
                                       <div class="form-group">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>   </div>
                                @endif


                                @if ($errors->has('password'))
                                <div class="form-group">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span></div>
                                @endif

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
