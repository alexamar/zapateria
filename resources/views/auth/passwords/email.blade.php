@extends('layouts.user')

@section('content')
<div class="fondo"></div>
<div class="btn-group">
  <button class="btn btn-sm dropdown-toggle btn-l" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      {{ __('message.leng') }}
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="{{ route('change_lang', ['lang' => 'es']) }}">ES</a>
    <a class="dropdown-item" href="{{ route('change_lang', ['lang' => 'en']) }}">EN</a>
  </div>
</div>
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="panel panel-default">
    
                <div class="panel-heading my-4"><h5 class="text-center text-white"><b> {{ __('message.forgetpassword') }}</b></h5></div>
                <div class="panel-body">
            

                     <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-12 control-label text-white" style="text-align: center;">    {{ __('message.email') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong style="color: white !important;">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                <button type="submit" class="btn btn-r btn-block">
                                       {{ __('message.send') }}
                                    </button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
