@extends('admin.template')
@php
$proveedor = DB::table('proveedor')->get();
$categoria = DB::table('categoria')->get();
$color = DB::table('color')->get();
@endphp

@section('contenido')
    <div class="row my-4">
         <div class="col-md-9">
            <h1 style="margin-left: 45%">AGREGAR PRODUCTOS</h1>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-md-12">
            <form class="row" method="POST" action="{{ route('mercancia.store') }}" id="form1">
                {{ csrf_field() }}

                 <div class="form-group row col-6" >
                    <label for="proveedor" class="col-sm-10 col-form-label" >Proveedor:</label>
                    <div class="col-sm-10">
                        <select class="custom-select" id="proveedor" name="proveedor" required>
                          @foreach($proveedor as $prov)
                           <option value="{{$prov->id}}">{{$prov->nombre}}</option>
                          @endforeach   
                        </select>
                    </div>
                </div>

                 <div class="form-group row col-6">
                    <label for="categoria" class="col-sm-10 col-form-label">Categoria:</label>
                    <div class="col-sm-10">
                        <select class="custom-select" id="categoria" name="categoria" required>
                          @foreach($categoria as $cat)
                           <option value="{{$cat->id}}">{{$cat->nombre}}</option>
                          @endforeach   
                        </select>
                    </div>
                </div>

                <div class="form-group row col-6">
                    <label for="color" class="col-sm-10 col-form-label">Color:</label>
                    <div class="col-sm-10">
                        <select class="custom-select" id="color" name="color" required>
                          @foreach($color as $col)
                           <option value="{{$col->id}}">{{$col->nombre}}</option>
                          @endforeach   
                        </select>
                    </div>
                </div>



                <div class="form-group row col-6" >

                    <label for="precio" class="col-sm-5 col-form-label" >Precio:</label>
                    <div class="col-sm-10">
                        <input   value="{{old('precio')}}" type="text" class="form-control" id="precio" name="precio" placeholder="Precio" required>


                    </div>
                </div>

                 <div class="form-group row col-6" >

                    <label for="talla" class="col-sm-5 col-form-label" >Talla:</label>
                    <div class="col-sm-10">
                        <input   value="{{old('talla')}}" type="text" class="form-control" id="talla" name="talla" placeholder="Talla" required>


                    </div>
                </div>
                  
                   <div class="form-group row col-6" >

                    <label for="cantidad" class="col-sm-5 col-form-label" >Cantidad:</label>
                    <div class="col-sm-10">
                        <input   value="{{old('cantidad')}}" type="text" class="form-control" id="cantidad" name="cantidad" placeholder="Cantidad" required>


                    </div>
                </div>
                              
                       
                <div class="form-group row col-6" >
                    <label for="Descripcion" class="col-sm-5 col-form-label" style="margin-left: 86%">Descripcion:</label>
                    <div class="col-sm-10">
                        <textarea  form="form1" maxlength="400" placeholder="descripcion" name="descripcion" required cols="80" rows="3" style="margin-left: 70%" autofocus="true">
                           
                             <?php
                                echo old('descripcion');
                             ?>

                        </textarea>
                        
                      
                    </div>
                </div>
               
                <div class="form-group row col-11 justify-content-center">
                    <button type="submit" class="btn btn-primary bt-md">Registrar</button>
                </div>
            </form>
        </div>
    </div>
    
    <script>
        


    </script>
@endsection