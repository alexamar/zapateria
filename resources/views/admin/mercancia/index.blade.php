@extends('admin.template')
@section('contenido')
  
<div class="row my-4">
    <div class="col-md-9">
            <h1 style="margin-left: 51%">LISTA DE ZAPATOS</h1>
        </div>

    <div class="col">
            <a style="float: right; margin-right: 5px" class="btn btn-success btn-icon" href="{{ route('mercancia.create') }}">
                <i  class="material-icons d-inline-block align-top">add</i>
                Agregar Nuevo Zapato
            </a>
        </div>
 
 </div>
    <div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            <form class="form-inline" type="POST" action="{{ route('producto.search') }}">
                <label class="sr-only" for="inlineFormInput">Nombre</label>
                <input type="text"class="form-control mb-2 mr-sm-2 mb-sm-0" name="producto" id="producto"  required>
                <button type="submit" class="btn btn-success">Buscar</button>
            </form>
        </div>
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Color</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Proveedor</th>
                    <th scope="col">Talla</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data as $d)
                            
                        <tr>
                            <td>{{ $d->descripcion }}</td>
                            <td>{{ $d->nombre  }}</td>
                            <td>@php
                                $producto =App\producto::find($d->producto_id);  @endphp
                                 @foreach($producto->categorias as $c) 
                                {{$c->nombre}}
                                @php
                                break;
                                @endphp
                                   @endforeach
                          </td>
                          <td>
                                 @foreach($producto->proveedores as $p) 
                                {{$p->nombre}}
                                @php
                                break;
                                @endphp
                                   @endforeach
                          </td>
                            <td>{{  $d->talla}}</td>
                            <td>{{  $d->total}}</td>
                            <td>{{  $d->precio}}</td>
                              <td class="btn-action">
                                <a class="btn btn-warning btn-icon" href="{{ route('mercancia.edit',$d->producto_id) }}">
                                    <i  class="material-icons d-inline-block align-top">mode_edit</i>
                                    Editar
                                </a>
                                <a class="btn btn-danger btn-icon" href="{{ route('mercancia.delete',$d->producto_id) }}">
                                    <i class="material-icons d-inline-block align-top">delete</i>
                                    Eliminar
                                </a>
                            </td>
                          
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            {{ $data->links() }}
        </div>
    </div>

@endsection
