@extends('admin.template')
@section('contenido')
    <div class="row my-4">
        <div class="col-md-9">
            <h1 style="margin-left: 45%">LISTA DE EMPLEADOS</h1>
        </div>
        <div class="col">
            <a style="float: right; margin-right: 5px" class="btn btn-success btn-icon" href="{{ route('empleado.create') }}">
                <i  class="material-icons d-inline-block align-top">add</i>
                Agregar Nuevo Empleado
            </a>
        </div>
    </div>
 
    <div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            <form class="form-inline" type="POST" action="{{ route('empleado.search') }}">
                <label class="sr-only" for="inlineFormInput">Nombre</label>
                <input type="text"class="form-control mb-2 mr-sm-2 mb-sm-0" name="empleado" id="empleado"  required>
                <button type="submit" class="btn btn-success">Buscar</button>
            </form>
        </div>
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Cedula</th>
                    <th scope="col">Direccion</th>
                    <th scope="col">Telefono</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($empleados as $emp)
                  
                        <tr>
                            <td>{{ $emp->nombre }}</td>
                            <td>{{  $emp->apellido }}</td>
                            <td>{{  $emp->cedula}}</td>
                            <td>{{  $emp->direccion}}</td>
                            <td>{{  $emp->telefono}}</td>
                            <td class="btn-action">
                                <a class="btn btn-warning btn-icon" href="">
                                    <i  class="material-icons d-inline-block align-top">mode_edit</i>
                                    Editar
                                </a>
                                <a class="btn btn-danger btn-icon" href="">
                                    <i class="material-icons d-inline-block align-top">delete</i>
                                    Eliminar
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            {{ $empleados->links() }}
        </div>
    </div>

@endsection
