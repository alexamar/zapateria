@extends('admin.template')
@section('contenido')
    <div class="justify-content-center my-4" style="display: flex;width: 100%;">
              <h1>REPORTE DE ZAPATOS</h1>
  
       
    </div>
 
    <div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            <form class="form-inline" type="POST" action="{{ route('inventario.search') }}">
                <label class="sr-only" for="inlineFormInput">Nombre</label>
                <input  onchange="cambiar(this.value)"  type="text"class="form-control mb-2 mr-sm-2 mb-sm-0" name="producto" id="producto"  required>
                <button type="submit" class="btn btn-success">Buscar</button>
            </form>
        </div>
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Color</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Proveedor</th>
                    <th scope="col">Talla</th>
                    <th scope="col">Cantidad</th>
          
                </tr>
                </thead>
                <tbody>

                  @php   $sum=0;    @endphp
                    @foreach($data as $d)
                           @php 
               
                    

                      $sum = $sum + $d->total;

                     @endphp
                        <tr>
                            <td>{{ $d->descripcion }}</td>
                            <td>{{ $d->nombre  }}</td>
                            <td>@php
                                $producto =App\producto::find($d->producto_id);  @endphp
                                 @foreach($producto->categorias as $c) 
                                {{$c->nombre}}
                                   @endforeach
                          </td>
                          <td>
                                 @foreach($producto->proveedores as $p) 
                                {{$p->nombre}}
                                   @endforeach
                          </td>
                            <td>{{  $d->talla}}</td>
                            <td>{{  $d->total}}</td>
                          
                          
                        </tr>
                    @endforeach
                </tbody>
                  <tr>
                  <th colspan="5" class="text-right" >TOTAL PRODUCTOS</th>
                   <th id = "suma">{{ $sum}}</th>     </tr>
                    <tr>
            </table>
        </div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
             <form class="form-inline" type="POST" action="{{ route('inventario.reporte') }}">
                     @foreach($data as $d)
                  <input hidden type="text"class="form-control mb-2 mr-sm-2 mb-sm-0" name="data[]" id="data[]" value={{$d->producto_id}}>
                   @endforeach
             <button type="submit" class="btn btn-success">Imprimir Reporte</button>
             </form>

            {{ $data->links() }}
        </div>
    </div>

@endsection
@section('js')
<script>


            
     function cambiar(val) {

        $("#pdf").val(document.getElementById("producto").value
 

            }



</script>
@endsection