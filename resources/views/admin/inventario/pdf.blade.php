@extends('admin.pdftemplate')
@section('contenido')
    <div class="justify-content-center my-2">
             <h3 class="subtitulo" style="text-align:center;">Reporte de Inventario</h3>
             <h4 class="subtitulo" style="text-align:center;">Zapateria Tiendas Lauren's</h4>
            <h4 class="subtitulo" style="text-align:center;">Fecha: {{ $fecha }}</h4>
    </div>
 
    <div>
       
        <div class="col-md-12" style="margin-top: 5%">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Color</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Proveedor</th>
                    <th scope="col">Talla</th>
                    <th scope="col">Cantidad</th>
          
                </tr>
                </thead>
                <tbody>

                  @php   $sum=0;    @endphp
                    @foreach($data as $d)
                           @php 
               
                    

                      $sum = $sum + $d->total;

                     @endphp
                        <tr>
                            <td>{{ $d->descripcion }}</td>
                            <td>{{ $d->nombre  }}</td>
                            <td>@php
                                $producto =App\producto::find($d->producto_id);  @endphp
                                 @foreach($producto->categorias as $c) 
                                {{$c->nombre}}
                                   @endforeach
                          </td>
                          <td>
                                 @foreach($producto->proveedores as $p) 
                                {{$p->nombre}}
                                   @endforeach
                          </td>
                            <td>{{  $d->talla}}</td>
                            <td>{{  $d->total}}</td>
                          
                          
                        </tr>
                    @endforeach
                </tbody>
                  <tr>
                  <th colspan="5" class="text-right" >TOTAL PRODUCTOS</th>
                   <th id = "suma">{{ $sum}}</th>     </tr>
                    <tr>
            </table>
        </div>
     
    </div>

@endsection
