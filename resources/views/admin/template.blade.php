@extends('layouts.app')

@section('content')



<!-- Iconos -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<nav class="navbar navbar-expand-lg navbar-light">

                <div class="items">

  <a alt="Inicio"  class="icon">  <img  src="{{ asset('img/icon.png') }}">  </a> 
  <a style="padding: 10px;"href="{{ route('empleado.index') }}" class="btn btn-success" >Empleados
    <i width="30" height="30" class="material-icons d-inline-block align-top">people</i></a>
    <a style="padding: 10px;"href="{{ route('proveedor.index') }}" class="btn btn-success" >Proveedores
    <i width="30" height="30" class="material-icons d-inline-block align-top">local_shipping</i></a>
    <a style="padding: 10px;"href="{{ route('mercancia.index') }}" class="btn btn-success" >Mercancia
    <i width="30" height="30" class="material-icons d-inline-block align-top">storage</i></a>
        <button class="btn btn-success" type="button">Categorias
          <i width="30" height="30" class="material-icons d-inline-block align-top">palette</i></button>
           <a style="padding: 10px;"href="{{ route('inventario.index') }}" class="btn btn-success" >Reportes de Inventario
                  <i width="30" height="30" class="material-icons d-inline-block align-top">print</i></a>
                       
                 <a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                       <button style="padding: 10px;" class="btn btn-success" type="button">Salir <i width="30" height="30" class="material-icons d-inline-block align-top">arrow_back</i></button>
                        </a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
                  
          </div>
       



</nav>
<div>


    <!-- Page Content -->

        <main>
            
                @yield('contenido')
            
        </main>
  

</div>

@if(session('info'))
    <script> swal('{{ session('info')['title'] }}','{!!  session('info')['text'] !!}','{{ session('info')['type'] }}')</script>
@elseif(session('error'))
    <script> swal('{{ session('error')['title'] }}','{!!  session('error')['text'] !!}','{{ session('error')['type'] }}')</script>
@elseif(session('warning'))
    <script> swal('{{ session('warning')['title'] }}','{!!  session('warning')['text'] !!}','{{ session('warning')['type'] }}')</script>
@endif



@endsection
@section('js')
@yield('js')
@endsection
