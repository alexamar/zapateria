@extends('admin.template')
@section('contenido')
    <div class="row my-4">
         <div class="col-md-9">
            <h1 style="margin-left: 45%">EDICION PROVEEDOR</h1>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-md-12">
            <form class="row" method="POST" action="{{ route('proveedor.update') }}" id="form1">
                {{ csrf_field() }}
                <input hidden name="id" value="{{ $proveedor->id }}">

                <div class="form-group row col-6" >

                      
                             
                    <label for="nombre" class="col-sm-5 col-form-label" >Nombre:</label>
                    <div class="col-sm-10">
                        <input   value="{{ $proveedor->nombre }}" type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>


                    </div>
                </div>
                  
                                <div class="form-group row col-6">
                 
                    <label for="identificacion" class="col-sm-5 col-form-label">Identificacion:</label>
                    <div class="col-sm-10">
                        <input   value="{{ $proveedor->numero_identificacion }}" type="text" class="form-control" id="numero_identificacion" name="numero_identificacion" placeholder="Identificacion" required>
                         @if ($errors->has('numero_identificacion'))
                                        <span class="help-block">
                                     {{ $errors->first('numero_identificacion') }}
                                    </span>
                                    @endif
                    </div>
                  
                </div>
                       
                <div class="form-group row col-6" >
                    <label for="Descripcion" class="col-sm-5 col-form-label" style="margin-left: 86%">Descripcion:</label>
                    <div class="col-sm-10">
                        <textarea  form="form1" name="descripcion"  style="margin-left: 70%" cols="80" >
                            <?php
                                echo $proveedor->descripcion;
                             ?>
                        </textarea>
                        
                      
                    </div>
                </div>
               
                <div class="form-group row col-11 justify-content-center">
                    <button type="submit" class="btn btn-primary bt-md">Registrar</button>
                </div>
            </form>
        </div>
    </div>
    
    <script>
        


    </script>
@endsection