@extends('layouts.app')

@section('content')



<!-- Iconos -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<div>


    <!-- Page Content -->

        <main>
            
                @yield('contenido')
            
        </main>
  

</div>




@endsection
@section('js')
@yield('js')
@endsection
