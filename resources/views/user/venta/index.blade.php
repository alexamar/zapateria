@extends('user.template')
@section('contenido')
    <div class="justify-content-center my-4" style="display: flex;width: 100%;">
              <h1>GESTIONAR VENTA</h1>
  
       
    </div>   
    <div class="justify-content-center" style="display: flex;width: 100%;">
      <h5><b>Ingresar Cliente</b></h5> 

    </div>  
      <div class="form-group">
     <div class="col-md-8">
  <div class="form-row">
    <div class="col">
      <input value="{{old('cedula')}}"  type="text" class="form-control" placeholder="Cedula" id="cedula" nombre="cedula">
    </div>
    <div class="col">
      <input type="text" class="form-control" placeholder="Nombre"  id="nombre" nombre="nombre" disabled>
    </div>

  </div>
  </div>
    </div>
          <div class="form-group">
     <div class="col-md-8">
  <div class="form-row">
    <div class="col">
      <input type="text" class="form-control" id="direccion" nombre="direccion" placeholder="Direccion" disabled>
    </div>
    <div class="col">
      <input type="text" class="form-control" id="telefono" nombre="telefono" placeholder="Telefono " disabled>
    </div>
      
  </div>
  </div>
    </div>
      <div class="col">
          <button  type="button" class="btn btn-info">Ingresar Nuevo Cliente</button>
              </div>
    <div>
       <div class="justify-content-center" style="display: flex;width: 100%;">
      <h5><b>Agregar Mercancia</b></h5> 

    </div>  
   <div class="form-group">
    <div class="justify-content-center" style="display: flex;">
                   <form class="form-inline" type="POST" action="{{ route('venta.agregar') }}">

                    <div class="col">
              <select data-placeholder="Seleccione los transportadores..."  name="producto" id="producto"  class="chosen-select form-control">
               @foreach($productos as $d)
                <option value="{{ $d->producto_id }}+{{ $d->descripcion }}+{{$d->nombre }}+{{$d->talla}}" id="{{ $d->producto_id }}+{{ $d->descripcion }}+{{$d->nombre }}+{{$d->talla}}">
                    {{ $d->producto_id }} {{ $d->descripcion }} {{$d->nombre }} {{$d->talla}}  </option>
            
            @endforeach

                      
          </select> 
              </div>
                <div class="col">
           <button type="submit" class="btn btn-success">Agregar</button>
           </div>
          </form>
        </div>
         </div>        <form method="POST"  action="{{ route('venta.store') }}">
        <div class="col-md-12">
        

        {{ csrf_field() }}
          <div class="justify-content-center" style="display: flex;width: 100%;">
      <h5><b>Listar Mercancia</b></h5> 
    </div> 
            <table  class="table table-striped">
                <thead>
                <tr>
                     <th scope="col">Accion</th>
                    <th scope="col">Codigo</th>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Color</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Proveedor</th>
                    <th scope="col">Talla</th>
                    <th scope="col">Cantidad</th>
                     <th scope="col">Precio</th>
                      <th scope="col">Total</th>
          
                </tr>
                </thead>
                <tbody id="mytable">
                    @php $cont = 0; 
                     $sum=0;
                      foreach ($data as $d) {  
                         $sum=$sum+$d->precio;
                        }

                    @endphp
                    @foreach($data as $d)
                            
                        <tr>
                              <td class="btn-action">

                                <a class="btn btn-danger btn-icon" href="{{ route('venta.delete', $cont) }}">
                                    X
                                </a>
                            </td>
                                 <td>{{ $d->producto_id }}</td>
                            <td>{{ $d->descripcion }}</td>
                            <td>{{ $d->nombre  }}</td>
                            <td>@php
                                $producto =App\producto::find($d->producto_id);  @endphp
                                 @foreach($producto->categorias as $c) 
                                {{$c->nombre}}
                                   @endforeach
                          </td>
                          <td>
                                 @foreach($producto->proveedores as $p) 
                                {{$p->nombre}}
                                   @endforeach
                          </td>
                            <td>{{  $d->talla}}</td>
                            <td style="width:60px"> <input  onchange="cambiarValor(this.value,{{ $cont }})" id="cantidad[]" name="cantidad[]" min="1" max={{ $d->total }} type="number"class="form-control" style="width:60px" value = 1></td>
                            <td class="precio">{{  $d->precio}}</td>
                            <td class="total">{{  $d->precio}}</td>
                        </tr>
                         @php $cont = $cont + 1; @endphp
                    @endforeach
                    </tbody>
     
                  <th colspan="9" class="text-right" >TOTAL A PAGAR</th>
                   <th id = "suma">${{ $sum }}</th>
                
            </table>
                <div class="col-md-10" style="direction: rtl; float:right;">
             <div class="form-group"  style="float: right; margin-right: 15px"> 
             <button  type="button" onclick="aceptar()" id="btn-aceptar" class="btn btn-info">Aceptar</button>
          </div>  </div>

    <div id="pago" class="col-md-10" style="direction: rtl; float:right;">
                <div class="form-group"  style="float: right; margin-right: 10px"> <h5><b>Metodo de pago</b></h5>    </div> 

           
                 <div class="form-group">
    
 <div class="col-md-2">
 
        <select class="form-control" id="tipopago" name="tipopago">
      <option>Tarjeta</option>
      <option>Efectivo</option>
 
    </select>
      </div>  </div>
  
    <div id="efectivo">
                 <div class="form-group">
 <div class="col-md-2">
      <input type="text" onchange="cambiarMontoE(this.value)"  class="form-control" id="montoE" name="montoE" placeholder="Monto Dado">
    </div>
</div>

  


         <div class="form-group"  style="text-align: right; margin-right: 10px"> <span id = "monto"></span> </div>

           </div> <input hidden type="text" class="form-control" id="cedula2" name="cedula2">
         <div class="form-group"  style="float: right; margin-right: 15px"> 
             <button  id="btn-pagar" type="submit" class="btn btn-info">Pagar</button>
          </div>
        </div>
     
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
        
        </div>
     
    </div>
      </form>

@endsection
@section('js')
<script>

 $(".chosen-select").chosen(); 
        function cambiarValor(val,c) {


            cantidad = val;

            precio = document.getElementById("mytable").rows[c].cells[8].innerText;

          document.getElementById("mytable").rows[c].cells[9].innerHTML = cantidad*precio

          var suma = 0

                              $('#mytable tr').each(function() {
                        var precio = $(this).find(".total").html();    
                       suma = suma + parseInt(precio);
                    
                     });

                              document.getElementById("suma").innerHTML = "$"+ suma

            }
     function cambiarMontoE(val) {


          valor = val;

               var suma = 0

                              $('#mytable tr').each(function() {
                        var precio = $(this).find(".total").html();    
                       suma = suma + parseInt(precio);
                    
                     });
              var montoE = parseInt(val) - suma

        

                   if(montoE < 0){
                       document.getElementById("btn-pagar").disabled = true;
                              document.getElementById("monto").innerHTML = "Monto No Valido"
                     } else{
                      document.getElementById("btn-pagar").disabled = false;
                        document.getElementById("monto").innerHTML = "Monto a devolver $"+ montoE
                     }
                      

            }
     function aceptar() {

         var pago = document.getElementById("pago");
                pago.style.display= "block";
        
            }



         $(document).ready(function() { 



         var pago = document.getElementById("pago");
                pago.style.display= "none";

document.getElementById("btn-aceptar").disabled = true;
document.getElementById("btn-pagar").disabled = false;

    $("#cedula").on('change',function(event){
      $.get("/test/public/cajero/cliente/"+event.target.value+"", function(response, state){
          $("#nombre").val(response.nombre);
          $("#telefono").val(response.telefono);
          $("#direccion").val(response.direccion);

                          var nombre= document.getElementById("nombre").value;
                               var cedula2= document.getElementById("cedula2").value;
                @foreach ($data as $d) {
                  if(nombre != ""){ 
                 document.getElementById("btn-aceptar").disabled = false;
                   $("#cedula2").val(document.getElementById("cedula").value);
               
                }
                }
                @endforeach
   
      });
    });

var nombre= document.getElementById("nombre").value;
@foreach ($data as $d) {
  if(nombre != ""){ 
 document.getElementById("btn-aceptar").disabled = false;
}
}
@endforeach
var tipopago = document.getElementById("tipopago");
var efectivo = document.getElementById("efectivo") ; 
//var input3 = document.getElementById("tipoCargaNueva");

   efectivo.style.display= "none";
tipopago.addEventListener("change", cambiarData2);

function cambiarData2(event) {
  if (tipopago.value == 'Efectivo') {
   // Mostrar
   efectivo.style.display= "block";
 document.getElementById("btn-pagar").disabled = true;
  } 
  else{
    // ocultar
   efectivo.style.display= "none";
document.getElementById("btn-pagar").disabled = false;

  }

}



        }); 

</script>
@endsection