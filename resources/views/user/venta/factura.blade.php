@extends('user.template')
@section('contenido')
    <div class="justify-content-center my-4" style="display: flex;width: 100%;">
              <h2>Visualizar Factura {{ $factura->id }}</h2>
  
       
    </div>
 
    <div>
       
        <div class="col-md-12">
            <div class="justify-content-center" style="display: flex;width: 100%;">
      <h5><b>Lista Productos Facturados</b></h5> 
    </div> 
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Color</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Proveedor</th>
                    <th scope="col">Talla</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Precio</th> 
                    <th scope="col">Total</th>
          
                </tr>
                </thead>
                <tbody>
                    @php   $sum=0;    @endphp
                    @foreach($productos as $pr)
                     @php 
               
                        $d= DB::table('producto')
                         ->where('id',$pr->producto_id)
                         ->get()->first(); 

                         $precio =   $d->precio*$pr->cantidad;

                      $sum = $sum + $precio;

                     @endphp
                            
                        <tr>
                            <td>{{ $d->descripcion }}</td>
                            <td>{{ $pr->color  }}</td>
                            <td>{{  $pr->producto_id}}@php
                                $producto =App\producto::find($pr->producto_id);  @endphp
                                 @foreach($producto->categorias as $c) 
                                {{$c->nombre}}
                                   @endforeach
                          </td>
                          <td>
                                 @foreach($producto->proveedores as $p) 
                                {{$p->nombre}}
                                   @endforeach
                          </td>
                            <td>{{  $pr->talla}}</td>
                            <td>{{  $pr->cantidad}}</td>
                             <td>{{  $d->precio}}</td>
                            <td>{{  $precio}}</td>
                          
                          
                        </tr>
                    @endforeach
                </tbody>
                    @php   $iva = $sum *0.12; $sub = $sum -$iva    @endphp
                     <tr>
                  <th colspan="7" class="text-right" >SUBTOTAL</th>
                   <th id = "suma">${{ $sub}}</th>     </tr>
                    <tr>
                  <th colspan="7" class="text-right" >IVA (12%)</th>
                   <th id = "suma">${{ $iva}}</th>     </tr>
                 <tr>  <th colspan="7" class="text-right" >TOTAL PAGADO</th>
                   <th id = "suma">${{ $sum }}</th> </tr>
            </table>
        </div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%;">
            
        </div>
    </div>

@endsection
