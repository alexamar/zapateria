@extends('user.template')
@section('contenido')
    <div class="justify-content-center my-4" style="display: flex;width: 100%;">
              <h1>CIERRE DE CAJA</h1>       
    </div>
 
    <div>
    
        <div class="justify-content-start my-2" style="display: flex;width: 100%;padding: 0px 0px 0px 20px ">
            @php $sum=0; @endphp
                @foreach($sql as $s)
                    @php
                        $precio = $s->cantidad * $s->precio;
                        $sum = $sum + $precio;
                    @endphp
                @endforeach
                <h4>El monto total para el cierre de caja hoy es: @php echo $sum; @endphp Bs.</h4>       
        </div>
       
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Fecha</th>
                    <th scope="col">Cajero</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Producto</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Precio</th>
          
                </tr>
                </thead>
                <tbody>
               
                @foreach($sql as $s)
                    <tr>
                        <td>{{ $s->fecha }}</td>
                        <td>{{ $s->empleado }}</td>
                        <td>{{ $s->nombre}}</td>
                        <td>{{ $s->descripcion }}</td>
                        <td>{{ $s->cantidad }}</td>
                        <td>{{ $s->precio }}</td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <div class="justify-content-end my-2" style="display: flex;width: 100%;padding: 0px 190px">            
            
            <a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                       <button style="padding: 10px;" class="btn btn-success" type="button">Cierre de caja <i width="30" height="30" class="material-icons d-inline-block align-top">arrow_back</i></button>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>


        </div>
        <div class="justify-content-center my-2" style="display: flex;width: 100%; ">
            {{ $sql->links() }}
        </div>
    </div>

@endsection
