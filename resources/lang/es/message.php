<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'leng' => 'Lenguaje',
    'forget' => '¿Olvidaste tu contraseña?',
    'password' => 'Contraseña',
    'login' => 'Ingresar',
     'signin' => ' Registrarse',
     'name' => 'Nombre',
     'country' => 'Pais',
     'confirmed' => 'Confirmar Contraseña',
      'register' => 'Registro',
        'selectcountry' => 'selecciona tu pais ',
         'forgetpassword' => 'Recuperacion de Contraseña',
       'send' => 'Enviar',
       'email' => 'Ingresa tu correo electrónico',
          'confirmedemail' => 'Bienvenido a Efecticarga',
            'confirmedemail_1' =>  'Hola, gracias por registrarte en',
'confirmedemail_2' =>  'Para comenzar a usar su cuenta',
'confirmedemail_3' =>  'por favor confirme su correo electrónico haciendo click en el siguiente enlace',
  'confirmedemail_4' =>  'click para confirmar correo',
      'resetpassword' =>  'Resetear Contraseña',
    'resetpassword_1' =>  'Notificacion de recuperacion de contraseña',
'resetpassword_2' =>  'Estás recibiendo este correo porque hiciste una solicitud de recuperacion de contraseña para tu cuenta.',
'resetpassword_3' =>  'Recuperar contraseña',
  'resetpassword_4' =>  'Si no realizaste esta solicitud, no se requiere realizar ninguna otra acción.',
    
    'hi' =>  'Hola!',
     'salutation' => 'Saludos',

     'reg_success' => 'Registro exitoso',
     'reg_text' => 'Porfavor confirma tu correo para poder ingresar',
     'conf_text' => 'Has confirmado correctamente tu correo!',
     
       'reset_success' => 'Envio Exitoso',
      'reset_text' => 'Revisa tu correo electrónico',
      'is_confirmed_mail' => 'Tu correo no ha sido confirmado',
];