<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
      'leng' => 'Language',
    'forget' => 'Forgot Password?',
    'password' => 'Password',
    'login' => 'Log in',
    'signin' => 'Sign in',
     'name' => 'Name',
     'country' => 'Country',
     'confirmed' => 'Confirm Password',
      'register' => 'Register',
       'selectcountry' => 'select your country ',
      'forgetpassword' => 'Forgotten Password',
         'send' => 'Send',
           'email' => 'Enter your email address',
       'confirmedemail' => 'Welcome to Efecticarga',
  'confirmedemail_1' =>  'Hi, thanks for register in',
'confirmedemail_2' =>  'In order to start using your account',
'confirmedemail_3' =>  'please confirm your email address by clicking the following link',
  'confirmedemail_4' =>  'click to confirm email',
'resetpassword' =>  'Reset Password',
  'resetpassword_1' =>  'Password reset notification',
'resetpassword_2' =>  'You are receiving this email because we received a password reset request for your account.',
'resetpassword_3' =>  'Reset Password',
  'resetpassword_4' =>  'If you did not request a password reset, no further action is required.',

  'hi' =>  'Hi!',
  'salutation' => 'Regards',

      'reg_success' => 'Registration successful',
      'reg_text' => 'Please confirm your email in order to access your account',
     'conf_text' => 'You have successfully confirmed your email!',

      'reset_success' => 'Email sent successfully',
      'reset_text' => 'Check your email',

      'is_confirmed_mail' => 'Your email has not been confirmed',


];
